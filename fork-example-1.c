/* fork-example-1.c */

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

void* create_shared_memory(size_t size) {
  return mmap(NULL,
              size,
              PROT_READ | PROT_WRITE,
              MAP_SHARED | MAP_ANONYMOUS,
              -1, 0);
}


int main() {
  
  int* arr = create_shared_memory(10 * sizeof(int));
  printf("Shared memory at: %p\n" , arr);
  
  for (int i = 0; i < 10; i++) *(arr + i) = i + 1;
  int pid = fork();

  if (pid == 0) {
    int x, ind;
    scanf("%d%d",&ind, &x);
    *(arr + ind) = x;
    printf("child finished, x=%d\n", x);
  } else {
  
    wait(NULL);
    printf("parent started\n");
    for (int i = 0; i < 10; i++) {
      printf("%d: %d\n", i, *(arr + i));
    }
  }
}
