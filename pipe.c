/* pipe-example.c */

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

void* create_shared_memory(size_t size) {
  return mmap(NULL,
              size,
              PROT_READ | PROT_WRITE,
              MAP_SHARED | MAP_ANONYMOUS,
              -1, 0);
}

int main() {
  // Создадим два конвейера
  int pipes[2][2];
  pipe(pipes[0]);
  pipe(pipes[1]);
  
  int* arr = create_shared_memory(10 * sizeof(int));
  printf("Shared memory at: %p\n" , arr);
  
  for (int i = 0; i < 10; i++) *(arr + i) = i + 1;
  // Создадим дочерний процесс
  pid_t pid = fork();
  if (pid == 0) {
      // Сохраним нужные дескпиторы конвейеров
      int to_parent_pipe = pipes[1][1];
      int from_parent_pipe = pipes[0][0];
      // И закроем ненужные
      close(pipes[1][0]);
      close(pipes[0][1]);
      
      int ind, x;
      while(1) {
        scanf("%d", &ind);
        if (ind < 0) break;
        scanf("%d", &x);
        *(arr+ind) = x;
        char c = 'A';
        write(to_parent_pipe, &c, 1);
        
        printf("send 1 to parent\n");
      }
      
      char c = 'B';
      write(to_parent_pipe, &c, 1);
      
      close(to_parent_pipe);
      close(from_parent_pipe);
      return 0;
  }
  else {
      // Сохраним нужные дескпиторы конвейеров
      int from_child_pipe = pipes[1][0];
      int to_child_pipe = pipes[0][1];
      // И закроем ненужные
      close(pipes[1][1]);
      close(pipes[0][0]);
      // Будем ждать, пока ребенок не пришлет 1 байт
      char c;
      while (1) {
        read(from_child_pipe, &c, 1);
        if (c == 'A') {
          printf("received 1 from child\n");
     
          // И отправим ответ
          for (int i = 0; i < 10; i++) {
            printf("%d: %d\n", i, *(arr + i));
          }
        }
        if (c == 'B') break;
      }
      
      // Дождемся завершения ребенка
      waitpid(pid, NULL, 0);
      // Закроем дескпиторы
      close(from_child_pipe);
      close(to_child_pipe);
      return 0;
  }
}
