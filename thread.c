#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

void* my_thread(void* arg)
{
  for(int i = 0; i < 10; i++ )
  {
    printf("%s\n", arg);
    sleep(1);
  }
return NULL;
}

int main()
{
  pthread_t t1, t2;
  pthread_create(&t1, NULL, my_thread, "Hello");
  pthread_create(&t2, NULL, my_thread, "world");
  pthread_exit( NULL );
  return 0;
}
