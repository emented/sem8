#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <semaphore.h>
#include <pthread.h>

extern int errno;

void* create_shared_memory(size_t size) {
    return mmap(NULL,
                size,
                PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_ANONYMOUS,
                -1, 0);
}

sem_t parent_semaphore;
sem_t child_semaphore;
sem_t exit_semaphore;

void* thread_1(int* arg) {
    int index, new_number;

    while(1) {
        sem_wait(&parent_semaphore);
        scanf("%d", &index);
        if (index < 0 || index >= 10) {
            sem_post(&exit_semaphore);
            printf("send 1 to parent\n");
            break;
        }
        scanf("%d", &new_number);
        arg[index] = new_number;
        sem_post(&child_semaphore);
    }

    sem_destroy(&child_semaphore);

    printf("end of child\n");
}

void* thread_2(int* arg) {
    while(1) {
        if(sem_trywait(&child_semaphore) == 0) {
            printf("received from child\n");
            for (int i = 0; i < 10; i++) {
              printf("%d: %d\n", i, *(arg + i));
            }
            sem_post(&parent_semaphore);
        }
        if(sem_trywait(&exit_semaphore) == 0) {
            sem_destroy(&exit_semaphore);
            break;
        }
    }

    sem_destroy(&parent_semaphore);

    printf("end of parent\n");

    return 0;
}

int main() {

    int* arr = create_shared_memory(128);

    for (int i = 0; i < 10; i++) {
        *(arr + i) = i + 1;
    }

    sem_init(&child_semaphore, 0, 0);
    sem_init(&parent_semaphore, 0, 1);
    sem_init(&exit_semaphore, 0, 0);

    pthread_t t1, t2;
    pthread_create(&t1, NULL, thread_1, arr);
    pthread_create(&t2, NULL, thread_2, arr);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    return 0;
}
